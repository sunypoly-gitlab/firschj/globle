# Midterm - CS 490 - Git, Docker, Kubernetes, CI/CD Pipelines - Spring 2022

This midterm is to completed by students at any time from Tuesday, March 8,
2022 at 6:00PM until Monday, March 14 at 12:00PM (noon). All work is to be
completed in a new GitLab repository specifically for this midterm. Please
commit often. Work is to be submitted by creating a merge request to your own
master/main/trunk branch from your feature branch, or branches, with my review
requested. Your work will only be considered submitted on time if the merge
request(s) is/are submitted with my review requested prior to the deadline. No
commits after the deadline will be considered.

## Tips

1. Create a merge request with my review requested as soon as you make the
   branch. This will prevent you from forgetting to do so by the deadline.
1. Keep the documentation open for whichever tool you are working with. This
   goes for Git, Docker, and CI/CD Pipelines.
1. None of the questions should take more than 3 sentences at most. Many can be
   answered in one.

## Git Questions (20 Points)

1. What information is tied to a commit?
1. What do all branches of a repository share? (Aside from the host repository)
1. When should a branch be created?
1. What causes a merge conflict?
1. How do you copy exactly one commit from one branch in a repository to
   another? You may assume the commit ID is `$COMMIT_ID`

## Docker Questions (20 Points)

1. What is the advantage of containers over running on the host system?
1. Explain one benefit of Docker compose files.
1. How many ENTRYPOINT lines can exist in a Dockerfile? How many are actually
   executed?
1. When would you want to use `ENTRYPOINT` **and** `CMD` together?
1. When is it acceptable to use the `latest` tag? (Hint: not a trick question).

## CI/CD Questions (20 Points)

1. If a job has a line showing `needs: []`, when will it run?
1. What does the `when` instruction do?
1. If I am trying to restrict a job to run only if the branch name starts with
   `verified-`, what CI instruction do I need?
1. Is it possible to override GitLab CI default variables?
1. What purpose does providing two cache keys serve?

## Docker Implementation (20 Points)

In this section you will be creating a Dockerfile to containerize a node-based
web game, [Globle](https://github.com/the-abe-train/globle). The documentation
for this repository is **very** simple. Please refer to it for build and run
instructions. This Dockerfile should meet all of the below requirements.

### Requirements

- [ ] Contain one stage
- [ ] **NOT** run as the root user
- [ ] Use `npm` to build the website
- [ ] Use `npm` to start the webpage when a container is created **always**
- [ ] Be accessible both in and out of the container on port 3000.
- [ ] A compose file should be in the repository alongside the Dockerfile.

## CI/CD Implementation (20 Points)

Expanding on the Docker implementation, create a CI/CD pipeline for the
[Globle](https://github.com/the-abe-train/globle) repository. This pipeline
should meet the following requirements.

### Requirements

- [ ] Contain four stages
- [ ] In the first stage, install the dependencies of the site
- [ ] In the second stage, run `npm doctor` to test the environment. Allow this
      to fail **with warning**.
- [ ] In the third stage, build and push the container image for the Dockerfile
      you built in the previous part.
- [ ] In the fourth stage, create a release **if** a commit tag has been
      pushed.

## Bonus (5 Points)

Looking ahead into the upcoming Kubernetes unit, this question is entirely
based on your ability to read documentation.

I have the following Docker command. Convert it to a Kubernetes (`kubectl`)
command.

```sh
docker run -it --rm --name my_ubuntu ubuntu:20.04
```



----

Globle README:

# Notes about Globle

**Version 1.3.0** - [Change log](CHANGELOG.md)

## Listed countries
- The list of countries for this game is the same as that used by [Sporcle](https://www.sporcle.com/blog/2013/01/what-is-a-country/)
- Some alternate spellings and previous names are accepted, e.g. Burma for Myanmar.
- France and UK have many territories scattered throughout the globe, and they confuse the proximity algorithm, so they are not highlighted when those countries are guessed.
- Geography can be a sensitive topic, and some countries' borders are disputed. If you believe a correction should be made, please politely raise an issue or DM me on Twitter.

## Tip
If you are really struggling to find the answer, I recommend going to Google Maps or Earth. Better to learn about a new country than never get the answer!

## Accessibility
I recognize that there are some a11y issues with the game, notably:
1. The colour gradient can be difficult to navigate for people with vision impairments, and
2. Very small countries (such as Micronesia) are barely visible on the globe.
If you have a suggestion on how to improve these concerns, please raise a GitHub issue.

## Analytics
Globle is hosted by CloudFlare and uses CloudFlare Web Analytics (CWA), a privacy-first analytics platform, to track user activity. The analytics are injected during hosting, so while you may see network traffic to https://cloudflareinsights.com/, the script that sends information to CWA is not anywhere in this repo. To learn more about CWA, [click here](https://www.cloudflare.com/en-gb/web-analytics/).

## Attributions
- This game was inspired by Wordle and the "Secret Country" geography games from [Sporcle](https://sporcle.com)
- Country outlines in the Help screen provided by Vemaps.com
- Favicons are from favicon.io

# Running the project on your local machine
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). If you want to run the project on your local machine,
1. Clone this repo
2. `npm install`
3. `npm start`

# License
Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
